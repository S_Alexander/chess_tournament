# Chess round-robin tournament prediction

Python script for predicting round-robin chess tournament results based on rating and played games. Script outputs probabilities of each players to place all possible places, performance rating, expected final placement and score.

At the moment the following order of tie-breaks is used:
1. Score;
2. Direct encounter;
3. Number of wins;
4. Sonneborn–Berger.


## Running example

```
git clone https://ashtanko@bitbucket.org/ashtanko/chess_tournament.git
cd .\chess_tournament
python .\roundrobin.py example
```

## Input file format

Input file is a cross table. Each row consists of name, rating and then N (number of players) values for game scores separated by tabs. Diagonal values of the table should be marked `-`. Values for unplayed games should be simply missing (number of tabs must be still appropriate).

This file format is useful because that's how cells are copied from google sheets.

## Output explanation

First the script outputs a table with the following columns:
1. Name;
2. Rat - Rating;
3. Perf - Performance rating;
4. 1st-Nth - probability of each player to take exactly this place;
5. Exp Pl - mathematical expectancy of finishing place;
6. Exp Sc - mathematical expectancy of finishing score.

Second it ouputs expected final placement (player names ordered by Exp Pl).

## Command line options

1. Script can take several input files:
```
python .\roundrobin.py file1 file2 file3
```
2. Flag `-n` specifies number of simulations. More - better, but slower.
3. When `--update` specified script updates ratings from lichess.
4. `-p` controls how probability of win/draw/loss for a single game is calculated:
    1. simple - approximation of Elo, draw probability is constant 20%
    2. complex - accurate Elo, draw probability is constant 20%
    3. complexNoDraw - accurate Elo, draw probability is 0%