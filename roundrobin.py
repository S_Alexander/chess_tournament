import numpy as np
import argparse
from miscs import getRating, getResult
from math import isnan, isclose

DELTA = 0.1

def calcPerformance(players, table):
    """Calculate performance rating of players"""
    for i in range(len(table)):
        if isnan(players[i]['rating']):
            players[i]['perf'] = float('nan')
            continue
        perf = 0.
        n = 0.
        for j in range(len(table)):
            if ((i != j) and (not isnan(table[i, j])) and (not isnan(players[j]['rating']))):
                perf += (table[i, j] - 0.5)*800 + players[j]['rating']
                n += 1.
        if (n > DELTA):
            players[i]['perf'] = perf / n
        else:
            players[i]['perf'] = float('nan')

def readTable(path, update=False):
    """Read tournament table from file."""
    f = open(path, "r")
    players = []
    table = []
    for line in f:
        splits = line.strip('\n').split('\t')
        header = splits[:2]
        values = splits[2:]
        name = header[0]
        if (update):
            rating = getRating(name)
        else:
            rating = int(header[1])
        players.append((name, rating, 0., 0., 0., 0., 0., 0., 0., 0.))
        scores = []
        for v in values:
            if (v == '1'):
                scores.append(1.)
            elif (v == '0'):
                scores.append(0.)
            elif (v == '0.5'):
                scores.append(0.5)
            elif (v == '-'):
                scores.append(0.)
            else:
                scores.append(float('nan'))
        table.append(scores)
    table = np.array(table)
    fields = [('name', 'S20'), ('rating', 'f4'), ('perf', 'f4'), ('score', 'f4'),
              ('tieDirect', 'f4'), ('tieWins', 'f4'), ('tieSonBer', 'f4'),
              ('tieRandom', 'f4'), ('expPl', 'f4'), ('expSc', 'f4')]
    players = np.array(players, dtype=fields)
    calcPerformance(players, table)
    return players, table


def calcScore(players, table):
    """Calculate players score from tournament table."""
    players['score'] = table.sum(-1)


def calcDirectTie(players, table, group):
    """Calculate direct tie-breaker from table for group of players."""
    ix = np.ix_(group, group)
    players['tieDirect'][group] = table[ix].sum(-1)


def calcWinsTie(players, table):
    """Calculate wins tie-breaker."""
    players['tieWins'] = (np.abs(table - 1.0) < DELTA).sum(-1)


def calcSonBerTie(players, table):
    """Calculate Sonneborn–Berger tie-breaker."""
    players['tieSonBer'] = np.dot(table, players['score'])


def calcRandomTie(players, table):
    """
    Calculate random tie-breaker to handle situations when scores and all
    tie-breakers are equal.
    """
    players['tieRandom'] = np.random.rand(len(players))


def runSim(players, table, getResult, tiebreaks, vector):
    """Run simulation."""
    length = len(players)
    for v in range(len(vector['direct'])):
        i,j = vector['direct'][v]
        result = getResult(players[i]['rating'], players[j]['rating'])
        table[i, j] = result
        table[j, i] = 1 - result
    calcScore(players, table)
    for i in range(length):
        players[i]['expSc'] += players[i]['score']
    for tie in tiebreaks:
        if (tie == 'tieDirect'):
            plsIndSorted = np.argsort(players, order=['score'])[::-1]
            group = [plsIndSorted[0]]
            for i in range(1, length):
                prevScore = players[plsIndSorted[i-1]]['score']
                currScore = players[plsIndSorted[i]]['score']
                if (abs(prevScore - currScore) < DELTA):
                    group.append(plsIndSorted[i])
                else:
                    calcDirectTie(players, table, group)
                    group = [plsIndSorted[i]]
            calcDirectTie(players, table, group)
        elif (tie == 'tieWins'):
            calcWinsTie(players, table)
        elif (tie == 'tieSonBer'):
            calcSonBerTie(players, table)
        elif (tie == 'tieRandom'):
            calcRandomTie(players, table)
    return players, table


def bar(percent):
    filler = int(20*percent)
    filled = '#'*filler
    empty = ' '*(20-filler)
    print("\rProgress: [{0}{1}] {2}%".format(filled, empty, int(100*percent)), end='', flush=True)


def runExp(players, table, getResult, trials):
    """Run experiment consisting of trials number of simulations."""
    tiebreaks = ['tieDirect', 'tieWins', 'tieSonBer', 'tieRandom']
    length = len(players)
    cross = np.zeros((length, length))
    vector = {'direct':[],'opposite':[]}
    for i in range(length):
        for j in range(i + 1, length):
            if isnan(table[i, j]):
                vector['direct'].append((i,j))
                vector['opposite'].append((j,i))
    step = trials // 100
    for t in range(trials):
        if (t % step == 0):
            bar(t / trials)
        pls, field = runSim(players, table, getResult, tiebreaks, vector)
        standings = np.argsort(pls, order=['score'] + tiebreaks)[::-1]
        for i in range(length):
            cross[standings[i], i] += 1.
    bar(1)
    print()
    for i in range(length):
        players[i]['expSc'] /= trials
    return (cross / trials)


ordinal = lambda n: str(n) +{1:'st', 2:'nd', 3:'rd'}.get(n % 10, 'th')


def outputTable(players, cross):
    """Output probability table."""
    header = ''
    header += '{0: <21}{1: <5}{2: <5}'.format("Name", "Rat", "Perf")
    for i in range(len(cross)):
        header += '{0: <6}'.format(ordinal(i+1))
    header += '| {0: <7}{1: <6}'.format("Exp Pl", "Exp Sc")
    print(header)
    output = ''
    for i in range(len(cross)):
        name = players[i]['name'].decode()
        rating = players[i]['rating']
        perf = players[i]['perf']
        output += '{0: <21}{1: <5.0f}{2: <5.0f}'.format(name, rating, perf)
        prob = ''
        for j in range(len(cross)):
            prob += '{0: <6.3f}'.format(cross[i][j])
        expPl = players[i]['expPl']
        expSc = players[i]['expSc']
        output += '{0}| {1: <7.3f}{2: <6.3f}\n'.format(prob, expPl, expSc)
    output += '\n'
    print(output)

def outputSortedPlayers(players):
    """Output players sorted by mathematical expectation of final placement."""
    sorted = np.sort(players, order='expPl')['name']
    places = range(1, len(sorted)+1)
    for p, name in np.stack((places, sorted), axis=-1):
        print("{0}. {1}".format(p.decode(), name.decode()))
    print()

def calcExpectedPos(players, cross):
    """Calculate mathematical expectation of final placement."""
    for i in range(len(cross)):
        players[i]['expPl'] = np.multiply(range(len(cross)), cross[i]).sum()+1


def processFile(file, n, update, calc):
    """Apply algorithm to file."""
    players, table = readTable(file, update=update)
    np.set_printoptions(precision=3, suppress=True)
    cross = runExp(players, table, getResult=getResult[calc], trials=n)
    calcExpectedPos(players, cross)
    outputTable(players, cross)
    outputSortedPlayers(players)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("files", nargs='+',
                        help="paths to files to be processed")
    parser.add_argument("-n", default=10000, type=int,
                        help="number of simulations")
    parser.add_argument("--update", action='store_true',
                        help="update ratings from lichess")
    parser.add_argument("-p", default='simple',
                        choices=['simple','complex','complexNoDraw'],
                        help="way to calculate win/loss probability")
    args = parser.parse_args()
    for file in args.files:
        processFile(file, args.n, args.update, args.p)


if __name__ == "__main__":
    main()
