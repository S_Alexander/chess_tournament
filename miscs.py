import requests
import json
import time
import numpy as np

TIMEOUT = 1
LONG_TIMEOUT = 60


def getRating(username):
    """Retrieve rating by username from lichess."""
    url = "https://lichess.org/api/user/" + username
    while True:
        time.sleep(TIMEOUT)
        response = requests.get(url)
        if (response.ok):
            data = json.loads(response.content)
            if (data.get('closed', False) is True):
                return float('nan')
            return int(data['perfs']['classical']['rating'])
        elif (response.too_many):
            time.sleep(LONG_TIMEOUT)
        else:
            print("Bad response: {0}".format(response.status_code))
            exit()


def resultSimple(rating1, rating2):
    """
    Assign random result proportionate to winning probabilities. Expected
    score is calculated with approximation. Draw probability is constant 20%.
    """
    perf = ((rating1 - rating2) / 850.) + 0.5
    draw = 0.2
    win = perf - draw / 2
    # loss = 1 - draw - win
    rnd = np.random.rand()
    if (rnd < draw):
        return 0.5
    elif (rnd < draw + win):
        return 1.0
    else:
        return 0.0


def resultComplex(rating1, rating2):
    """
    Assign random result proportionate to winning probabilities. Expected
    score is calculated accurately. Draw probability is constant 20%.
    """
    m = (rating2 - rating1) / 400.
    perf = 1. / (1 + 10**m)
    draw = 0.2
    win = perf - draw / 2
    # loss = 1 - draw - win
    rnd = np.random.rand()
    if (rnd < draw):
        return 0.5
    elif (rnd < draw + win):
        return 1.0
    else:
        return 0.0


def resultComplexNoDraw(rating1, rating2):
    """
    Assign random result proportionate to winning probabilities. Expected
    score is calculated accurately. No draws allowed.
    """
    m = (rating2 - rating1) / 400.
    win = 1. / (1 + 10**m)
    rnd = np.random.rand()
    if (rnd < win):
        return 1.0
    else:
        return 0.0


getResult = {'simple':resultSimple,
             'complex':resultComplex,
             'complexNoDraw':resultComplexNoDraw }
